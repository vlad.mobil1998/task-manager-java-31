package service;

import marker.UnitCategory;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Task;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.service.ServiceLocator;

@Ignore
public class TaskServiceTest {

    private static IServiceLocator serviceLocator;

    private static ITaskService taskService;

    private static IProjectService projectService;

    private static User user;

    private static User user1;

    private static Task task;

    private static Task task1;

    private static Project project;

    private static Project project1;

    @BeforeClass
    public static void addTaskData() throws Exception{
        serviceLocator = new ServiceLocator();
        serviceLocator.getPropertyServer().init();
        serviceLocator.getEntityManagerService().initTest();
        projectService = serviceLocator.getProjectService();
        taskService = serviceLocator.getTaskService();
        IUserService userService = serviceLocator.getUserService();
        userService.create("test", "test", Role.USER);
        userService.create("temp", "temp", Role.USER);
        user = userService.findByLogin("test");
        user1 = userService.findByLogin("temp");

        projectService.create(user.getId(), "testP");
        projectService.create(user1.getId(), "tempP");
        project = projectService.findOneByName(user.getId(), "testP");
        project1 = projectService.findOneByName(user1.getId(), "tempP");

        taskService.create(user.getId(), project.getId(), "test");
        taskService.create(user1.getId(), project1.getId(), "temp");
        task = taskService.findOneByName(user.getId(), "test");
        task1 = taskService.findOneByName(user1.getId(), "temp");
    }

    @Test
    @Category(UnitCategory.class)
    public void createAndFindTaskTest() {
        Task task, task1;
        taskService.create(user.getId(), project.getId(), "134");
        taskService.create(user1.getId(), project1.getId(), "341");
        task = taskService.findOneByName(user.getId(), "134");
        Assert.assertEquals("134", task.getName());
        Assert.assertEquals(user.getId(), task.getUser().getId());

        task1 = taskService.findOneByName(user1.getId(), "341");
        Assert.assertEquals("341", task1.getName());
        Assert.assertEquals(user1.getId(), task1.getUser().getId());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void createTaskTestExceptionEmpty() {
        taskService.create("", "ds", "sd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void createTaskTestExceptionNull() {
        taskService.create(null, "sd", "ad");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void createTaskTestExceptionEmpty1() {
        taskService.create("1", "", "ds");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void createTaskTestExceptionNull1() {
        taskService.create("1", null, "sds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionEmpty() {
        taskService.findOneByName("", "ds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionNull() {
        taskService.findOneByName(null, "sd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionEmpty1() {
        taskService.findOneByName("sd", "");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionNull1() {
        taskService.findOneByName("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        Task taskTest = taskService.findOneByName(user.getId(), "test");
        Assert.assertEquals(taskTest.getId(), task.getId());

        Task taskTest1 = taskService.findOneById(user1.getId(), task1.getId());
        Assert.assertEquals(taskTest1.getId(), task1.getId());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionEmpty2() {
        taskService.findOneById("", "sd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionNull1() {
        taskService.findOneById(null, "ds");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionEmpty() {
        taskService.findOneById("sds", "");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionNull() {
        taskService.findOneById("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTest() {
        taskService.removeOneById(user1.getId(), task1.getId());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionEmpty2() {
        taskService.removeOneById("", "sd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionNull1() {
        taskService.removeOneById(null, "ds");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionEmpty() {
        taskService.removeOneById("sds", "");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionNull() {
        taskService.removeOneById("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByNameTest() {
        taskService.create(user1.getId(), project1.getId(), "temp");
        taskService.removeOneByName(user1.getId(), "temp");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionEmpty() {
        taskService.removeOneByName("", "ds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionNull() {
        taskService.removeOneByName(null, "sd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionEmpty1() {
        taskService.removeOneByName("sd", "");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionNull1() {
        taskService.removeOneByName("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void updateTaskByIdTest() {
        Task task;
        taskService.create(user.getId(),project.getId(), "update");
        task = taskService.findOneByName(user.getId(), "update");

        taskService.updateTaskById(user.getId(), task.getId(), "testUp", "up");
        task1 = taskService.findOneByName(user.getId(), "testUp");

        Assert.assertEquals(task1.getName(), "testUp");
        Assert.assertEquals(task1.getDescription(), "up");
        Assert.assertEquals(task1.getId(), task.getId());
        Assert.assertEquals(task1.getUser().getId(), task.getUser().getId());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestExceptionEmpty() {
        taskService.updateTaskById("", "sd", "dsa", "saq");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestExceptionNull1() {
        taskService.updateTaskById(null, "ds", "we", "re");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestExceptionEmpty1() {
        taskService.updateTaskById("sds", "", "we", "qwe");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestExceptionNull() {
        taskService.updateTaskById("ds", null, "esa", "asd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestExceptionEmpty2() {
        taskService.updateTaskById("sds", "sd", "", "qwe");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestExceptionNull2() {
        taskService.updateTaskById("ds", "sda", null, "asd");
    }

    @AfterClass
    public static void exit() {
        serviceLocator.getUserService().clear();
    }

}