#import "task-manager-server.h"
#ifndef DEF_TASK_MANAGER_SERVERNS1AbstractEntity_M
#define DEF_TASK_MANAGER_SERVERNS1AbstractEntity_M

/**
 * (no documentation provided)
 */
@implementation TASK_MANAGER_SERVERNS1AbstractEntity

/**
 * (no documentation provided)
 */
- (NSString *) identifier
{
  return _identifier;
}

/**
 * (no documentation provided)
 */
- (void) setIdentifier: (NSString *) newIdentifier
{
  [newIdentifier retain];
  [_identifier release];
  _identifier = newIdentifier;
}

- (void) dealloc
{
  [self setIdentifier: nil];
  [super dealloc];
}
@end /* implementation TASK_MANAGER_SERVERNS1AbstractEntity */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_SERVERNS1AbstractEntity (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TASK_MANAGER_SERVERNS1AbstractEntity (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_SERVERNS1AbstractEntity (JAXB)

/**
 * Read an instance of TASK_MANAGER_SERVERNS1AbstractEntity from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_SERVERNS1AbstractEntity defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_SERVERNS1AbstractEntity *_tASK_MANAGER_SERVERNS1AbstractEntity = [[TASK_MANAGER_SERVERNS1AbstractEntity alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_SERVERNS1AbstractEntity initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_SERVERNS1AbstractEntity = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_SERVERNS1AbstractEntity autorelease];
  return _tASK_MANAGER_SERVERNS1AbstractEntity;
}

/**
 * Initialize this instance of TASK_MANAGER_SERVERNS1AbstractEntity according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_SERVERNS1AbstractEntity to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setIdentifier: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self identifier]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}id."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}id...");
#endif
    [[self identifier] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}id...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}id."];
    }
  }
}
@end /* implementation TASK_MANAGER_SERVERNS1AbstractEntity (JAXB) */

#endif /* DEF_TASK_MANAGER_SERVERNS1AbstractEntity_M */
#ifndef DEF_TASK_MANAGER_SERVERNS1Role_M
#define DEF_TASK_MANAGER_SERVERNS1Role_M

/**
 * Reads a Role from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Role, or NULL if unable to be read.
 */
static enum TASK_MANAGER_SERVERNS1Role *xmlTextReaderReadTASK_MANAGER_SERVERNS1RoleType(xmlTextReaderPtr reader)
{
  xmlChar *enumValue = xmlTextReaderReadEntireNodeValue(reader);
  enum TASK_MANAGER_SERVERNS1Role *value = calloc(1, sizeof(enum TASK_MANAGER_SERVERNS1Role));
  if (enumValue != NULL) {
    if (xmlStrcmp(enumValue, BAD_CAST "USER") == 0) {
      *value = TASK_MANAGER_SERVER_NS1_ROLE_USER;
      free(enumValue);
      return value;
    }
    if (xmlStrcmp(enumValue, BAD_CAST "ADMIN") == 0) {
      *value = TASK_MANAGER_SERVER_NS1_ROLE_ADMIN;
      free(enumValue);
      return value;
    }
#if DEBUG_ENUNCIATE
    NSLog(@"Attempt to read enum value failed: %s doesn't match an enum value.", enumValue);
#endif
  }
#if DEBUG_ENUNCIATE
  else {
    NSLog(@"Attempt to read enum value failed: NULL value.");
  }
#endif

  return NULL;
}

/**
 * Utility method for getting the enum value for a string.
 *
 * @param _role The string to format.
 * @return The enum value or NULL on error.
 */
enum TASK_MANAGER_SERVERNS1Role *formatStringToTASK_MANAGER_SERVERNS1RoleType(NSString *_role)
{
  enum TASK_MANAGER_SERVERNS1Role *value = calloc(1, sizeof(enum TASK_MANAGER_SERVERNS1Role));
  if ([@"USER" isEqualToString:_role]) {
    *value = TASK_MANAGER_SERVER_NS1_ROLE_USER;
  }
  else if ([@"ADMIN" isEqualToString:_role]) {
    *value = TASK_MANAGER_SERVER_NS1_ROLE_ADMIN;
  }
  else{
#if DEBUG_ENUNCIATE
  NSLog(@"Attempt to read enum value failed: %s doesn't match an enum value.", [_role UTF8String]);
#endif
    value = NULL;
  }
  return value;
}

/**
 * Writes a Role to XML.
 *
 * @param writer The XML writer.
 * @param _role The Role to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteTASK_MANAGER_SERVERNS1RoleType(xmlTextWriterPtr writer, enum TASK_MANAGER_SERVERNS1Role *_role)
{
  switch (*_role) {
    case TASK_MANAGER_SERVER_NS1_ROLE_USER:
      return xmlTextWriterWriteString(writer, BAD_CAST "USER");
    case TASK_MANAGER_SERVER_NS1_ROLE_ADMIN:
      return xmlTextWriterWriteString(writer, BAD_CAST "ADMIN");
  }

#if DEBUG_ENUNCIATE
  NSLog(@"Unable to write enum value (no valid value found).");
#endif
  return -1;
}

/**
 * Utility method for getting the string value of Role.
 *
 * @param _role The Role to format.
 * @return The string value or NULL on error.
 */
static NSString *formatTASK_MANAGER_SERVERNS1RoleTypeToString(enum TASK_MANAGER_SERVERNS1Role *_role)
{
  switch (*_role) {
    case TASK_MANAGER_SERVER_NS1_ROLE_USER:
      return @"USER";
    case TASK_MANAGER_SERVER_NS1_ROLE_ADMIN:
      return @"ADMIN";
    default:
      return NULL;
  }

  return NULL;
}
#endif /* DEF_TASK_MANAGER_SERVERNS1Role_M */
#ifndef DEF_TASK_MANAGER_SERVERNS1Domain_M
#define DEF_TASK_MANAGER_SERVERNS1Domain_M

/**
 * (no documentation provided)
 */
@implementation TASK_MANAGER_SERVERNS1Domain

/**
 * (no documentation provided)
 */
- (NSArray *) projects
{
  return _projects;
}

/**
 * (no documentation provided)
 */
- (void) setProjects: (NSArray *) newProjects
{
  [newProjects retain];
  [_projects release];
  _projects = newProjects;
}

/**
 * (no documentation provided)
 */
- (NSArray *) tasks
{
  return _tasks;
}

/**
 * (no documentation provided)
 */
- (void) setTasks: (NSArray *) newTasks
{
  [newTasks retain];
  [_tasks release];
  _tasks = newTasks;
}

/**
 * (no documentation provided)
 */
- (NSArray *) users
{
  return _users;
}

/**
 * (no documentation provided)
 */
- (void) setUsers: (NSArray *) newUsers
{
  [newUsers retain];
  [_users release];
  _users = newUsers;
}

- (void) dealloc
{
  [self setProjects: nil];
  [self setTasks: nil];
  [self setUsers: nil];
  [super dealloc];
}
@end /* implementation TASK_MANAGER_SERVERNS1Domain */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_SERVERNS1Domain (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TASK_MANAGER_SERVERNS1Domain (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_SERVERNS1Domain (JAXB)

/**
 * Read an instance of TASK_MANAGER_SERVERNS1Domain from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_SERVERNS1Domain defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_SERVERNS1Domain *_tASK_MANAGER_SERVERNS1Domain = [[TASK_MANAGER_SERVERNS1Domain alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_SERVERNS1Domain initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_SERVERNS1Domain = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_SERVERNS1Domain autorelease];
  return _tASK_MANAGER_SERVERNS1Domain;
}

/**
 * Initialize this instance of TASK_MANAGER_SERVERNS1Domain according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_SERVERNS1Domain to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "projects", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}projects of type {}project.");
#endif

     __child = [TASK_MANAGER_SERVERNS1Project readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}projects of type {}project.");
#endif

    if ([self projects]) {
      [self setProjects: [[self projects] arrayByAddingObject: __child]];
    }
    else {
      [self setProjects: [NSArray arrayWithObject: __child]];
    }
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "tasks", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}tasks of type {}task.");
#endif

     __child = [TASK_MANAGER_SERVERNS1Task readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}tasks of type {}task.");
#endif

    if ([self tasks]) {
      [self setTasks: [[self tasks] arrayByAddingObject: __child]];
    }
    else {
      [self setTasks: [NSArray arrayWithObject: __child]];
    }
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "users", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}users of type {}user.");
#endif

     __child = [TASK_MANAGER_SERVERNS1User readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}users of type {}user.");
#endif

    if ([self users]) {
      [self setUsers: [[self users] arrayByAddingObject: __child]];
    }
    else {
      [self setUsers: [NSArray arrayWithObject: __child]];
    }
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self projects]) {
    __enumerator = [[self projects] objectEnumerator];

    while ( (__item = [__enumerator nextObject]) ) {
      status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "projects", NULL);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing start child element {}projects."];
      }

#if DEBUG_ENUNCIATE > 1
      NSLog(@"writing element {}projects...");
#endif
      [__item writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
      NSLog(@"successfully wrote element {}projects...");
#endif

      status = xmlTextWriterEndElement(writer);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing end child element {}projects."];
      }
    } //end item iterator.
  }
  if ([self tasks]) {
    __enumerator = [[self tasks] objectEnumerator];

    while ( (__item = [__enumerator nextObject]) ) {
      status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "tasks", NULL);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing start child element {}tasks."];
      }

#if DEBUG_ENUNCIATE > 1
      NSLog(@"writing element {}tasks...");
#endif
      [__item writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
      NSLog(@"successfully wrote element {}tasks...");
#endif

      status = xmlTextWriterEndElement(writer);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing end child element {}tasks."];
      }
    } //end item iterator.
  }
  if ([self users]) {
    __enumerator = [[self users] objectEnumerator];

    while ( (__item = [__enumerator nextObject]) ) {
      status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "users", NULL);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing start child element {}users."];
      }

#if DEBUG_ENUNCIATE > 1
      NSLog(@"writing element {}users...");
#endif
      [__item writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
      NSLog(@"successfully wrote element {}users...");
#endif

      status = xmlTextWriterEndElement(writer);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing end child element {}users."];
      }
    } //end item iterator.
  }
}
@end /* implementation TASK_MANAGER_SERVERNS1Domain (JAXB) */

#endif /* DEF_TASK_MANAGER_SERVERNS1Domain_M */
#ifndef DEF_TASK_MANAGER_SERVERNS1Task_M
#define DEF_TASK_MANAGER_SERVERNS1Task_M

/**
 * (no documentation provided)
 */
@implementation TASK_MANAGER_SERVERNS1Task

/**
 * (no documentation provided)
 */
- (NSString *) name
{
  return _name;
}

/**
 * (no documentation provided)
 */
- (void) setName: (NSString *) newName
{
  [newName retain];
  [_name release];
  _name = newName;
}

/**
 * (no documentation provided)
 */
- (NSString *) description
{
  return _description;
}

/**
 * (no documentation provided)
 */
- (void) setDescription: (NSString *) newDescription
{
  [newDescription retain];
  [_description release];
  _description = newDescription;
}

/**
 * (no documentation provided)
 */
- (NSString *) userId
{
  return _userId;
}

/**
 * (no documentation provided)
 */
- (void) setUserId: (NSString *) newUserId
{
  [newUserId retain];
  [_userId release];
  _userId = newUserId;
}

- (void) dealloc
{
  [self setName: nil];
  [self setDescription: nil];
  [self setUserId: nil];
  [super dealloc];
}
@end /* implementation TASK_MANAGER_SERVERNS1Task */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_SERVERNS1Task (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TASK_MANAGER_SERVERNS1Task (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_SERVERNS1Task (JAXB)

/**
 * Read an instance of TASK_MANAGER_SERVERNS1Task from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_SERVERNS1Task defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_SERVERNS1Task *_tASK_MANAGER_SERVERNS1Task = [[TASK_MANAGER_SERVERNS1Task alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_SERVERNS1Task initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_SERVERNS1Task = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_SERVERNS1Task autorelease];
  return _tASK_MANAGER_SERVERNS1Task;
}

/**
 * Initialize this instance of TASK_MANAGER_SERVERNS1Task according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_SERVERNS1Task to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "name", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setName: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "description", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setDescription: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "userId", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setUserId: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self name]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "name", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}name."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}name...");
#endif
    [[self name] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}name...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}name."];
    }
  }
  if ([self description]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "description", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}description."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}description...");
#endif
    [[self description] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}description...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}description."];
    }
  }
  if ([self userId]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "userId", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}userId."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}userId...");
#endif
    [[self userId] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}userId...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}userId."];
    }
  }
}
@end /* implementation TASK_MANAGER_SERVERNS1Task (JAXB) */

#endif /* DEF_TASK_MANAGER_SERVERNS1Task_M */
#ifndef DEF_TASK_MANAGER_SERVERNS1User_M
#define DEF_TASK_MANAGER_SERVERNS1User_M

/**
 * (no documentation provided)
 */
@implementation TASK_MANAGER_SERVERNS1User

/**
 * (no documentation provided)
 */
- (NSString *) login
{
  return _login;
}

/**
 * (no documentation provided)
 */
- (void) setLogin: (NSString *) newLogin
{
  [newLogin retain];
  [_login release];
  _login = newLogin;
}

/**
 * (no documentation provided)
 */
- (NSString *) passwordHash
{
  return _passwordHash;
}

/**
 * (no documentation provided)
 */
- (void) setPasswordHash: (NSString *) newPasswordHash
{
  [newPasswordHash retain];
  [_passwordHash release];
  _passwordHash = newPasswordHash;
}

/**
 * (no documentation provided)
 */
- (NSString *) email
{
  return _email;
}

/**
 * (no documentation provided)
 */
- (void) setEmail: (NSString *) newEmail
{
  [newEmail retain];
  [_email release];
  _email = newEmail;
}

/**
 * (no documentation provided)
 */
- (NSString *) fistName
{
  return _fistName;
}

/**
 * (no documentation provided)
 */
- (void) setFistName: (NSString *) newFistName
{
  [newFistName retain];
  [_fistName release];
  _fistName = newFistName;
}

/**
 * (no documentation provided)
 */
- (NSString *) lastName
{
  return _lastName;
}

/**
 * (no documentation provided)
 */
- (void) setLastName: (NSString *) newLastName
{
  [newLastName retain];
  [_lastName release];
  _lastName = newLastName;
}

/**
 * (no documentation provided)
 */
- (NSString *) middleName
{
  return _middleName;
}

/**
 * (no documentation provided)
 */
- (void) setMiddleName: (NSString *) newMiddleName
{
  [newMiddleName retain];
  [_middleName release];
  _middleName = newMiddleName;
}

/**
 * (no documentation provided)
 */
- (enum TASK_MANAGER_SERVERNS1Role *) role
{
  return _role;
}

/**
 * (no documentation provided)
 */
- (void) setRole: (enum TASK_MANAGER_SERVERNS1Role *) newRole
{
  if (_role != NULL) {
    free(_role);
  }
  _role = newRole;
}

/**
 * (no documentation provided)
 */
- (BOOL *) locked
{
  return _locked;
}

/**
 * (no documentation provided)
 */
- (void) setLocked: (BOOL *) newLocked
{
  if (_locked != NULL) {
    free(_locked);
  }
  _locked = newLocked;
}

- (void) dealloc
{
  [self setLogin: nil];
  [self setPasswordHash: nil];
  [self setEmail: nil];
  [self setFistName: nil];
  [self setLastName: nil];
  [self setMiddleName: nil];
  [self setRole: NULL];
  [self setLocked: NULL];
  [super dealloc];
}
@end /* implementation TASK_MANAGER_SERVERNS1User */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_SERVERNS1User (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TASK_MANAGER_SERVERNS1User (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_SERVERNS1User (JAXB)

/**
 * Read an instance of TASK_MANAGER_SERVERNS1User from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_SERVERNS1User defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_SERVERNS1User *_tASK_MANAGER_SERVERNS1User = [[TASK_MANAGER_SERVERNS1User alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_SERVERNS1User initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_SERVERNS1User = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_SERVERNS1User autorelease];
  return _tASK_MANAGER_SERVERNS1User;
}

/**
 * Initialize this instance of TASK_MANAGER_SERVERNS1User according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_SERVERNS1User to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "login", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setLogin: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "passwordHash", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}passwordHash of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}passwordHash of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setPasswordHash: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "email", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}email of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}email of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setEmail: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "fistName", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}fistName of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}fistName of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setFistName: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "lastName", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}lastName of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}lastName of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setLastName: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "middleName", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}middleName of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}middleName of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setMiddleName: __child];
    return YES;
  } //end "if choice"


  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "role", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

    _child_accessor = xmlTextReaderReadTASK_MANAGER_SERVERNS1RoleType(reader);
    if (_child_accessor == NULL) {
      //panic: unable to return the value for some reason.
      [NSException raise: @"XMLReadError"
                   format: @"Error reading element value."];
    }
    [self setRole: ((enum TASK_MANAGER_SERVERNS1Role*) _child_accessor)];
    return YES;
  }

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "locked", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

    _child_accessor = xmlTextReaderReadBooleanType(reader);
    if (_child_accessor == NULL) {
      //panic: unable to return the value for some reason.
      [NSException raise: @"XMLReadError"
                   format: @"Error reading element value."];
    }
    [self setLocked: ((BOOL*) _child_accessor)];
    return YES;
  }

  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self login]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "login", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}login."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}login...");
#endif
    [[self login] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}login...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}login."];
    }
  }
  if ([self passwordHash]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "passwordHash", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}passwordHash."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}passwordHash...");
#endif
    [[self passwordHash] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}passwordHash...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}passwordHash."];
    }
  }
  if ([self email]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "email", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}email."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}email...");
#endif
    [[self email] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}email...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}email."];
    }
  }
  if ([self fistName]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "fistName", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}fistName."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}fistName...");
#endif
    [[self fistName] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}fistName...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}fistName."];
    }
  }
  if ([self lastName]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "lastName", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}lastName."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}lastName...");
#endif
    [[self lastName] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}lastName...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}lastName."];
    }
  }
  if ([self middleName]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "middleName", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}middleName."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}middleName...");
#endif
    [[self middleName] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}middleName...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}middleName."];
    }
  }
  if ([self role] != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "role", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}role."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}role...");
#endif
    status = xmlTextWriterWriteTASK_MANAGER_SERVERNS1RoleType(writer, [self role]);
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}role...");
#endif
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing child element {}role."];
    }

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}role."];
    }
  }
  if ([self locked] != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "locked", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}locked."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}locked...");
#endif
    status = xmlTextWriterWriteBooleanType(writer, [self locked]);
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}locked...");
#endif
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing child element {}locked."];
    }

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}locked."];
    }
  }
}
@end /* implementation TASK_MANAGER_SERVERNS1User (JAXB) */

#endif /* DEF_TASK_MANAGER_SERVERNS1User_M */
#ifndef DEF_TASK_MANAGER_SERVERNS1Session_M
#define DEF_TASK_MANAGER_SERVERNS1Session_M

/**
 * (no documentation provided)
 */
@implementation TASK_MANAGER_SERVERNS1Session

/**
 * (no documentation provided)
 */
- (long long *) timestamp
{
  return _timestamp;
}

/**
 * (no documentation provided)
 */
- (void) setTimestamp: (long long *) newTimestamp
{
  if (_timestamp != NULL) {
    free(_timestamp);
  }
  _timestamp = newTimestamp;
}

/**
 * (no documentation provided)
 */
- (NSString *) userId
{
  return _userId;
}

/**
 * (no documentation provided)
 */
- (void) setUserId: (NSString *) newUserId
{
  [newUserId retain];
  [_userId release];
  _userId = newUserId;
}

/**
 * (no documentation provided)
 */
- (NSString *) signature
{
  return _signature;
}

/**
 * (no documentation provided)
 */
- (void) setSignature: (NSString *) newSignature
{
  [newSignature retain];
  [_signature release];
  _signature = newSignature;
}

- (void) dealloc
{
  [self setTimestamp: NULL];
  [self setUserId: nil];
  [self setSignature: nil];
  [super dealloc];
}
@end /* implementation TASK_MANAGER_SERVERNS1Session */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_SERVERNS1Session (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TASK_MANAGER_SERVERNS1Session (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_SERVERNS1Session (JAXB)

/**
 * Read an instance of TASK_MANAGER_SERVERNS1Session from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_SERVERNS1Session defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_SERVERNS1Session *_tASK_MANAGER_SERVERNS1Session = [[TASK_MANAGER_SERVERNS1Session alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_SERVERNS1Session initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_SERVERNS1Session = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_SERVERNS1Session autorelease];
  return _tASK_MANAGER_SERVERNS1Session;
}

/**
 * Initialize this instance of TASK_MANAGER_SERVERNS1Session according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_SERVERNS1Session to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "timestamp", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

    _child_accessor = xmlTextReaderReadLongType(reader);
    if (_child_accessor == NULL) {
      //panic: unable to return the value for some reason.
      [NSException raise: @"XMLReadError"
                   format: @"Error reading element value."];
    }
    [self setTimestamp: ((long long*) _child_accessor)];
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "userId", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setUserId: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "signature", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}signature of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}signature of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setSignature: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self timestamp] != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "timestamp", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}timestamp."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}timestamp...");
#endif
    status = xmlTextWriterWriteLongType(writer, [self timestamp]);
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}timestamp...");
#endif
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing child element {}timestamp."];
    }

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}timestamp."];
    }
  }
  if ([self userId]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "userId", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}userId."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}userId...");
#endif
    [[self userId] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}userId...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}userId."];
    }
  }
  if ([self signature]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "signature", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}signature."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}signature...");
#endif
    [[self signature] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}signature...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}signature."];
    }
  }
}
@end /* implementation TASK_MANAGER_SERVERNS1Session (JAXB) */

#endif /* DEF_TASK_MANAGER_SERVERNS1Session_M */
#ifndef DEF_TASK_MANAGER_SERVERNS1Project_M
#define DEF_TASK_MANAGER_SERVERNS1Project_M

/**
 * (no documentation provided)
 */
@implementation TASK_MANAGER_SERVERNS1Project

/**
 * (no documentation provided)
 */
- (NSString *) name
{
  return _name;
}

/**
 * (no documentation provided)
 */
- (void) setName: (NSString *) newName
{
  [newName retain];
  [_name release];
  _name = newName;
}

/**
 * (no documentation provided)
 */
- (NSString *) description
{
  return _description;
}

/**
 * (no documentation provided)
 */
- (void) setDescription: (NSString *) newDescription
{
  [newDescription retain];
  [_description release];
  _description = newDescription;
}

/**
 * (no documentation provided)
 */
- (NSString *) userId
{
  return _userId;
}

/**
 * (no documentation provided)
 */
- (void) setUserId: (NSString *) newUserId
{
  [newUserId retain];
  [_userId release];
  _userId = newUserId;
}

- (void) dealloc
{
  [self setName: nil];
  [self setDescription: nil];
  [self setUserId: nil];
  [super dealloc];
}
@end /* implementation TASK_MANAGER_SERVERNS1Project */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_SERVERNS1Project (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TASK_MANAGER_SERVERNS1Project (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_SERVERNS1Project (JAXB)

/**
 * Read an instance of TASK_MANAGER_SERVERNS1Project from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_SERVERNS1Project defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_SERVERNS1Project *_tASK_MANAGER_SERVERNS1Project = [[TASK_MANAGER_SERVERNS1Project alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_SERVERNS1Project initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_SERVERNS1Project = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_SERVERNS1Project autorelease];
  return _tASK_MANAGER_SERVERNS1Project;
}

/**
 * Initialize this instance of TASK_MANAGER_SERVERNS1Project according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_SERVERNS1Project to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "name", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setName: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "description", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setDescription: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "userId", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setUserId: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self name]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "name", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}name."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}name...");
#endif
    [[self name] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}name...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}name."];
    }
  }
  if ([self description]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "description", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}description."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}description...");
#endif
    [[self description] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}description...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}description."];
    }
  }
  if ([self userId]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "userId", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}userId."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}userId...");
#endif
    [[self userId] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}userId...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}userId."];
    }
  }
}
@end /* implementation TASK_MANAGER_SERVERNS1Project (JAXB) */

#endif /* DEF_TASK_MANAGER_SERVERNS1Project_M */
