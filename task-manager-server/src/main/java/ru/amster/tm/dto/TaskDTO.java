package ru.amster.tm.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Task;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Data
public final class TaskDTO {

    @NotNull
    private String id;

    @NotNull
    private String userId;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Date dataBegin;

    @NotNull
    private Date dataEnd;

    public TaskDTO(@Nullable final Task task) {
        if (task == null) return;
        id = task.getId();
        if (task.getUser() != null) userId = task.getUser().getId();
        description = task.getDescription();
        dataBegin = task.getDataBegin();
        dataEnd = task.getDataEnd();
    }

}