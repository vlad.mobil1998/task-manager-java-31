package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public final class EmptyProjectException extends AbstractException {

    public EmptyProjectException() {
        super("Error! Project is empty (not found)...");
    }

}