package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public class EmptySignatureException extends AbstractException {

    public EmptySignatureException() {
        super("ERROR! Signature is empty...");
    }

}