package ru.amster.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.amster.tm.api.servise.IEntityManagerService;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.Task;
import ru.amster.tm.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public final class EntityManagerService implements IEntityManagerService {

    @NotNull
    private ServiceLocator serviceLocator;

    @NotNull
    private EntityManagerFactory managerFactory;

    private EntityManagerFactory factory() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(
                Environment.DRIVER,
                serviceLocator.getPropertyServer().getJdbcDriver()
        );
        settings.put(
                Environment.URL,
                serviceLocator.getPropertyServer().getJdbcUrl()
        );
        settings.put(
                Environment.USER,
                serviceLocator.getPropertyServer().getJdbcUsername()
        );
        settings.put(
                Environment.PASS,
                serviceLocator.getPropertyServer().getJdbcPassword()
        );
        settings.put(
                Environment.DIALECT,
                "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "create");
        settings.put(Environment.SHOW_SQL, "true");

        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);

        final Metadata metadata = sources.getMetadataBuilder().build();
        EntityManagerFactory managerFactory
                = metadata.getSessionFactoryBuilder().build();
        return managerFactory;
    }

    private EntityManagerFactory factoryTest() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(
                Environment.DRIVER,
                serviceLocator.getPropertyServer().getJdbcDriver()
        );
        settings.put(
                Environment.URL,
                serviceLocator.getPropertyServer().getUrlTest()
        );
        settings.put(
                Environment.USER,
                serviceLocator.getPropertyServer().getJdbcUsername()
        );
        settings.put(
                Environment.PASS,
                serviceLocator.getPropertyServer().getJdbcPassword()
        );
        settings.put(
                Environment.DIALECT,
                "org.hibernate.dialect.MySQL5InnoDBDialect"
        );
        settings.put(Environment.HBM2DDL_AUTO, "create");
        settings.put(Environment.SHOW_SQL, "true");

        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);

        final Metadata metadata = sources.getMetadataBuilder().build();
        EntityManagerFactory managerFactory
                = metadata.getSessionFactoryBuilder().build();
        return managerFactory;
    }

    public EntityManagerService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void initTest() {
        managerFactory = factoryTest();
    }

    @Override
    public void init() {
        managerFactory = factory();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return managerFactory.createEntityManager();
    }

}