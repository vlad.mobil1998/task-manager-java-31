package ru.amster.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.exception.empty.*;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.User;
import ru.amster.tm.repository.UserRepository;
import ru.amster.tm.util.HashUtil;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@NoArgsConstructor
public final class UserService implements IUserService {

    @NotNull
    private IServiceLocator serviceLocator;

    public UserService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        add(user);
    }

    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        add(user);
    }

    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (role == null) throw new EmptyRoleException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        user.setRole(role);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        add(user);
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        User user;
        try {
            em.getTransaction().begin();
            IUserRepository userRepository = new UserRepository(em);
            user = userRepository.findById(id);
            return user;
        } catch (NoResultException e) {
            throw new EmptyUserException();
        } catch (RuntimeException e) {
            System.out.println("Message exception (User): " + e.getMessage());
            return null;
        }finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        User user;
        try {
            em.getTransaction().begin();
            IUserRepository userRepository = new UserRepository(em);
            user = userRepository.findByLogin(login);
            return user;
        } catch (NoResultException e) {
            throw new EmptyUserException();
        } catch (RuntimeException e) {
            System.out.println("Message exception (User): " + e.getMessage());
            return null;
        }finally {
            em.close();
        }
    }

    @Override
    public void removeUser(@Nullable final User user) {
        if (user == null) throw new EmptyUserException();
        removeById(user.getId());
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            IUserRepository userRepository = new UserRepository(em);
            userRepository.removeById(id);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (User): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            IUserRepository userRepository = new UserRepository(em);
            userRepository.removeByLogin(login);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void updateEmail(
            @Nullable final String id,
            @Nullable final String email
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();

        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            user.setEmail(email);
            em.merge(user);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (User): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void updateFirstName(
            @Nullable final String id,
            @Nullable final String firstName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();

        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            user.setFistName(firstName);
            em.merge(user);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (User): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void updateLastName(
            @Nullable final String id,
            @Nullable final String lastName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();

        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            user.setLastName(lastName);
            em.merge(user);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (User): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void updateMiddleName(
            @Nullable final String id,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();

        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            user.setMiddleName(middleName);
            em.merge(user);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (User): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void updatePassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        @Nullable final String passwordUpd = HashUtil.salt(password);
        if (passwordUpd == null) throw new EmptyPasswordException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            user.setPasswordHash(passwordUpd);
            em.merge(user);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (User): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable User user;
        user = findByLogin(login);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            user.setLocked(true);
            em.merge(user);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (User): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable User user;
        user = findByLogin(login);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            user.setLocked(false);
            em.merge(user);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (User): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void load(@NotNull final List<User> users) {
        clear();
        for (@NotNull User user: users) {
            add(user);
        }
    }

    @Override
    @Nullable
    public List<User> export() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            IUserRepository repository = new UserRepository(em);
            return repository.getEntity();
        } catch (RuntimeException e) {
            System.out.println("Message exception (User): " + e.getMessage());
            return null;
        }finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public List<User> findAll() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            IUserRepository repository = new UserRepository(em);
            return repository.getEntity();
        } catch (RuntimeException e) {
            System.out.println("Message exception (User): " + e.getMessage());
            return null;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable final User user) {
        if (user == null) throw new EmptyEntityException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            IUserRepository repository = new UserRepository(em);
            repository.merge(user);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (User): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void clear() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            IUserRepository repository = new UserRepository(em);
            repository.removeAll();
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (User): " + e.getMessage());
        } finally {
            em.close();
        }
    }

}