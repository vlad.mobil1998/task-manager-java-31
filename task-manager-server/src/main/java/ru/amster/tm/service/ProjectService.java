package ru.amster.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.*;
import ru.amster.tm.exception.system.ProjectCreated;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@NoArgsConstructor
public final class ProjectService implements IProjectService {

    @NotNull
    private IServiceLocator serviceLocator;

    public ProjectService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable Project project;
        @Nullable User user;

        try {
            user = serviceLocator.getUserService().findById(userId);
        } catch (EmptyUserException e) {
            throw new EmptyUserException();
        } catch (RuntimeException e) {
            System.out.println("Message exception (Project): " + e.getMessage());
            return;
        }

        try {
            project = findOneByName(userId, name);
            if (project != null) throw new ProjectCreated();
        } catch (EmptyProjectException e) {
            project = new Project(name, user);
            add(project);
        } catch (RuntimeException e) {
            System.out.println("Message exception (Project): " + e.getMessage());
        }
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;

        @Nullable User user;
        @Nullable Project project;

        try {
            user = serviceLocator.getUserService().findById(userId);
        } catch (EmptyProjectException e) {
            throw new EmptyUserException();
        } catch (RuntimeException e) {
            System.out.println("Message exception (Project): " + e.getMessage());
            return;
        }

        try {
            project = findOneByName(userId, name);
            if (project != null) throw new ProjectCreated();
        } catch (RuntimeException e) {
            project = new Project(name, user);
            project.setDescription(description);
            add(project);
        }
    }

    @Nullable
    @Override
    public Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        Project project;
        try {
            em.getTransaction().begin();
            IProjectRepository projectRepository = new ProjectRepository(em);
            project = projectRepository.findOneById(userId, id);
            return project;
        } catch (NoResultException e) {
            throw new EmptyProjectException();
        } catch (RuntimeException e) {
            System.out.println("Message exception (Project): " + e.getMessage());
            return null;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public Project findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        Project project;

        try {
            em.getTransaction().begin();
            IProjectRepository projectRepository = new ProjectRepository(em);
            project = projectRepository.findOneByName(userId, name);
            return project;
        } catch (NoResultException e) {
            throw new EmptyProjectException();
        } catch (RuntimeException e) {
            System.out.println("Message exception (Project): " + e.getMessage());
            return null;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            IProjectRepository projectRepository = new ProjectRepository(em);
            projectRepository.removeOneByName(userId, name);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Project): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();

        try {
            em.getTransaction().begin();
            IProjectRepository projectRepository = new ProjectRepository(em);
            projectRepository.removeOneById(userId, id);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Project): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        @Nullable Project project;
        try {
            em.getTransaction().begin();
            IProjectRepository projectRepository = new ProjectRepository(em);
            project = projectRepository.findOneById(userId, id);
            project.setId(id);
            project.setName(name);
            project.setDescription(description);
            em.merge(project);
            em.getTransaction().commit();
        } catch (NoResultException e){
            em.getTransaction().rollback();
            throw new EmptyProjectException();
        }catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Project): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public Long numberOfAllProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        @Nullable Long count;
        try {
            em.getTransaction().begin();
            IProjectRepository projectRepository = new ProjectRepository(em);
            count = projectRepository.numberOfAllProjects(userId);
            return count;
        } catch (RuntimeException e) {
            return null;
        } finally {
            em.close();
        }
    }

    @Override
    public void load(@NotNull final List<Project> projects) {
        clear();
        for (@NotNull Project project: projects) {
            add(project);
        }
    }

    @Override
    @Nullable
    public List<Project> export() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            IProjectRepository repository = new ProjectRepository(em);
            return repository.getEntity();
        } catch (RuntimeException e) {
            System.out.println("Message exception (Project): " + e.getMessage());
            return null;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            IProjectRepository repository = new ProjectRepository(em);
            return repository.getEntity();
        } catch (RuntimeException e) {
            System.out.println("Message exception (Project): " + e.getMessage());
            return null;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable final Project project) {
        if (project == null) throw new EmptyEntityException();

        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            IProjectRepository repository = new ProjectRepository(em);
            repository.merge(project);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            System.out.println("Message exception (Project): " + e.getMessage());
        } finally {
            em.close();
        }
    }

    @Override
    public void clear() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        try {
            em.getTransaction().begin();
            IProjectRepository repository = new ProjectRepository(em);
            repository.removeAll();
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

}