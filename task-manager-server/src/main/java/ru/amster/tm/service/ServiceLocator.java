package ru.amster.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.api.servise.*;
import ru.amster.tm.endpoint.*;

public final class ServiceLocator implements IServiceLocator {

    @NotNull
    private final EntityManagerService entityManagerService = new EntityManagerService(this);

    @NotNull
    private final IPropertyServer propertyServer = new PropertyServer();

    @NotNull
    private final IUserService userService = new UserService(this);

    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @NotNull
    private final IDomainService domainService = new DomainService(
            projectService, taskService, userService
    );

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    @Override
    public EntityManagerService getEntityManagerService() {
        return entityManagerService;
    }

    @NotNull
    @Override
    public AdminUserEndpoint getAdminUserEndpoint() {
        return adminUserEndpoint;
    }

    @NotNull
    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public IPropertyServer getPropertyServer() {
        return propertyServer;
    }

    @NotNull
    @Override
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

}