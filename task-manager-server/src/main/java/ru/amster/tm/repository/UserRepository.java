package ru.amster.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptyUserException;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

public final class UserRepository implements IUserRepository {

    @NotNull
    private EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<User> criteriaQuery
                = criteriaBuilder.createQuery(User.class);
        final Root<User> root = criteriaQuery.from(User.class);

        final Path<String> pathId = root.get("id");
        final ParameterExpression<String> parameterId
                = criteriaBuilder.parameter(String.class, "id");
        final Predicate predicate
                = criteriaBuilder.equal(pathId, parameterId);
        criteriaQuery.where(predicate);

        final TypedQuery<User> typedQuery
                = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("id", id);
        return typedQuery.getSingleResult();
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyIdException();

        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<User> criteriaQuery
                = criteriaBuilder.createQuery(User.class);
        final Root<User> root = criteriaQuery.from(User.class);

        final Path<String> pathLogin = root.get("login");
        final ParameterExpression<String> parameterLogin
                = criteriaBuilder.parameter(String.class, "login");
        final Predicate predicate
                = criteriaBuilder.equal(pathLogin, parameterLogin);
        criteriaQuery.where(predicate);

        final TypedQuery<User> typedQuery
                = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("login", login);
        return typedQuery.getSingleResult();
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyLoginException();
        entityManager.remove(entityManager.find(User.class, id));
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        entityManager.remove(entityManager.find(User.class, login));
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) throw new EmptyUserException();
        entityManager.merge(user);
    }

    @Override
    public void removeAll() {
        List<User> users = getEntity();
        for (User user: users) {
            entityManager.remove(user);
        }
    }

    @NotNull
    @Override
    public List<User> getEntity() {
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<User> criteriaQuery
                = criteriaBuilder.createQuery(User.class);
        criteriaQuery.from(User.class);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public void remove(@Nullable final User user) {
        if (user == null) throw new EmptyUserException();

        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaDelete<User> criteriaDelete
                = criteriaBuilder.createCriteriaDelete(User.class);
        final Root<User> root = criteriaDelete.from(User.class);

        final Path<User> pathId = root.get("user");
        final CriteriaDelete<User> parameterId
                = criteriaBuilder.createCriteriaDelete(User.class);
        final Predicate predicateId
                = criteriaBuilder.equal(pathId, parameterId);
        criteriaDelete.where(predicateId);

        final Query query = entityManager.createQuery(criteriaDelete);
        query.setParameter("user", user);
        query.executeUpdate();
    }

}