package ru.amster.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.user.AccessDeniedException;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;


public final class TaskRepository implements ITaskRepository {

    @NotNull
    private EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Task> criteriaQuery
                = criteriaBuilder.createQuery(Task.class);
        final Root<Task> root = criteriaQuery.from(Task.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "user");

        final Predicate predicate
                = criteriaBuilder.equal(pathUserId, parameterUserId);
        criteriaQuery.where(predicate);

        final TypedQuery<Task> typedQuery
                = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Task> criteriaDelete
                = criteriaBuilder.createCriteriaDelete(Task.class);
        final Root<Task> root = criteriaDelete.from(Task.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "user");

        final Predicate predicate
                = criteriaBuilder.equal(pathUserId, parameterUserId);
        criteriaDelete.where(predicate);

        final Query query = entityManager.createQuery(criteriaDelete);
        query.setParameter("user", userId);
        query.executeUpdate();
    }

    @Nullable
    @Override
    public Task findOneByName(
            @Nullable final String userId,
            @NotNull final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Task> criteriaQuery
                = criteriaBuilder.createQuery(Task.class);
        final Root<Task> root = criteriaQuery.from(Task.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "user");
        final Path<String> pathName = root.get("name");
        final ParameterExpression<String> parameterName
                = criteriaBuilder.parameter(String.class, "name");

        final Predicate[] predicates = new Predicate[2];
        predicates[0] = criteriaBuilder.and(
                criteriaBuilder.equal(pathUserId, parameterUserId)
        );
        predicates[1] = criteriaBuilder.and(
                criteriaBuilder.equal(pathName, parameterName)
        );
        criteriaQuery.where(predicates);

        final TypedQuery<Task> typedQuery
                = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("user", userId);
        typedQuery.setParameter("name", name);
        return typedQuery.getSingleResult();
    }

    @Nullable
    @Override
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Task> criteriaQuery
                = criteriaBuilder.createQuery(Task.class);
        final Root<Task> root = criteriaQuery.from(Task.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "user");
        final Path<String> pathName = root.get("id");
        final ParameterExpression<String> parameterName
                = criteriaBuilder.parameter(String.class, "id");

        final Predicate[] predicates = new Predicate[2];
        predicates[0] = criteriaBuilder.and(
                criteriaBuilder.equal(pathUserId, parameterUserId)
        );
        predicates[1] = criteriaBuilder.and(
                criteriaBuilder.equal(pathName, parameterName)
        );
        criteriaQuery.where(predicates);

        final TypedQuery<Task> typedQuery
                = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("user", userId);
        typedQuery.setParameter("id", id);
        return typedQuery.getSingleResult();
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Task> criteriaDelete
                = criteriaBuilder.createCriteriaDelete(Task.class);
        final Root<Task> root = criteriaDelete.from(Task.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "user");
        final Path<String> pathName = root.get("name");
        final ParameterExpression<String> parameterName
                = criteriaBuilder.parameter(String.class, "name");

        final Predicate[] predicates = new Predicate[2];
        predicates[0] = criteriaBuilder.and(
                criteriaBuilder.equal(pathUserId, parameterUserId)
        );
        predicates[1] = criteriaBuilder.and(
                criteriaBuilder.equal(pathName, parameterName)
        );
        criteriaDelete.where(predicates);

        final Query query = entityManager.createQuery(criteriaDelete);
        query.setParameter("user", userId);
        query.setParameter("name", name);
        query.executeUpdate();
    }

    @Override
    public void removeOneById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Task> criteriaDelete
                = criteriaBuilder.createCriteriaDelete(Task.class);
        final Root<Task> root = criteriaDelete.from(Task.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "user");
        final Path<String> pathName = root.get("id");
        final ParameterExpression<String> parameterName
                = criteriaBuilder.parameter(String.class, "id");

        final Predicate[] predicates = new Predicate[2];
        predicates[0] = criteriaBuilder.and(
                criteriaBuilder.equal(pathUserId, parameterUserId)
        );
        predicates[1] = criteriaBuilder.and(
                criteriaBuilder.equal(pathName, parameterName)
        );
        criteriaDelete.where(predicates);

        final Query query = entityManager.createQuery(criteriaDelete);
        query.setParameter("user", userId);
        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    @NotNull
    public Long numberOfAllTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();

        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Long> criteriaQuery
                = criteriaBuilder.createQuery(Long.class);
        final Root<Task> root = criteriaQuery.from(Task.class);
        criteriaQuery.select(criteriaBuilder.count(root));

        final Path<String> pathId = root.get("user").get("id");
        final ParameterExpression<String> parameterId
                = criteriaBuilder.parameter(String.class, "user");
        final Predicate predicateById
                = criteriaBuilder.equal(pathId, parameterId);
        criteriaQuery.where(predicateById);

        final TypedQuery<Long> typedQuery
                = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("user", userId);
        return typedQuery.getSingleResult();
    }

    @NotNull
    @Override
    public List<Task> getEntity() {
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Task> criteriaQuery
                = criteriaBuilder.createQuery(Task.class);
        criteriaQuery.from(Task.class);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public void removeAll() {
        List<Task> tasks = getEntity();
        for (Task task: tasks) {
            entityManager.remove(task);
        }
    }

    @Override
    public void remove(@NotNull final Task task) {
        if (task == null) throw new EmptyTaskException();

        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Task> criteriaDelete
                = criteriaBuilder.createCriteriaDelete(Task.class);
        final Root<Task> root = criteriaDelete.from(Task.class);

        final Path<Task> pathId = root.get("task");
        final CriteriaDelete<Task> parameterId
                = criteriaBuilder.createCriteriaDelete(Task.class);
        final Predicate predicateId
                = criteriaBuilder.equal(pathId, parameterId);
        criteriaDelete.where(predicateId);

        final Query query = entityManager.createQuery(criteriaDelete);
        query.setParameter("task", task);
        query.executeUpdate();
    }

    @Override
    public void merge(@NotNull final Task record) {
        entityManager.merge(record);
    }

}