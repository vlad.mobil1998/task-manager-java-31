package ru.amster.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.ISessionRepository;
import ru.amster.tm.entity.Session;
import ru.amster.tm.exception.empty.EmptyEntityException;
import ru.amster.tm.exception.user.AccessDeniedException;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;


public class SessionRepository implements ISessionRepository {

    @NotNull
    private EntityManager entityManager;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public List<Session> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        
        final CriteriaBuilder criteriaBuilder
                = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Session> criteriaQuery
                = criteriaBuilder.createQuery(Session.class);
        final Root<Session> root = criteriaQuery.from(Session.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "id");
        final Predicate predicate = criteriaBuilder.equal(pathUserId, parameterUserId);
        criteriaQuery.where(predicate);

        final TypedQuery<Session> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("id", userId);
        return typedQuery.getResultList();
    }

    @Override
    public void removeByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Session> criteriaDelete
                = criteriaBuilder.createCriteriaDelete(Session.class);
        final Root<Session> root = criteriaDelete.from(Session.class);

        final Path<String> pathUserId = root.get("user").get("id");
        final ParameterExpression<String> parameterUserId
                = criteriaBuilder.parameter(String.class, "id");
        final Predicate predicate = criteriaBuilder.equal(pathUserId, parameterUserId);
        criteriaDelete.where(predicate);

        final Query query = entityManager.createQuery(criteriaDelete);
        query.setParameter("id", userId);
        query.executeUpdate();
    }

    @Override
    @NotNull
    public Boolean contains(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new AccessDeniedException();

        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Long> criteriaQuery
                = criteriaBuilder.createQuery(Long.class);
        final Root<Session> root = criteriaQuery.from(Session.class);
        criteriaQuery.select(criteriaBuilder.count(root));

        final Path<String> pathId = root.get("id");
        final ParameterExpression<String> parameterId
                = criteriaBuilder.parameter(String.class, "id");
        final Predicate predicate = criteriaBuilder.equal(pathId, parameterId);
        criteriaQuery.where(predicate);

        final TypedQuery<Long> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("id", id);
        Long result = typedQuery.getSingleResult();
        if (result == 1) return true;
        return false;
    }

    @NotNull
    @Override
    public List<Session> getEntity() {
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Session> criteriaQuery
                = criteriaBuilder.createQuery(Session.class);
        criteriaQuery.from(Session.class);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public void remove(@Nullable final Session session) {
        if(session == null) throw new EmptyEntityException();

        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Session> criteriaQuery
                = criteriaBuilder.createCriteriaDelete(Session.class);
        final Root<Session> root = criteriaQuery.from(Session.class);

        final Path<Session> pathId = root.get("session");
        final ParameterExpression<Session> parameterId
                = criteriaBuilder.parameter(Session.class, "session");
        final Predicate predicateId = criteriaBuilder.equal(pathId, parameterId);
        criteriaQuery.where(predicateId);

        final Query query = entityManager.createQuery(criteriaQuery);
        query.setParameter("session", session);
        query.executeUpdate();
    }

    @Override
    public void merge(@Nullable final Session session) {
        if(session == null) throw new EmptyEntityException();
        entityManager.merge(session);
    }

    @Override
    public void removeAll() {
        List<Session> sessions = getEntity();
        for (Session session: sessions) {
            entityManager.remove(session);
        }
    }

}