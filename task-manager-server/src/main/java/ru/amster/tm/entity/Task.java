package ru.amster.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Task implements Serializable {

    @ManyToOne
    @NotNull
    private Project project;

    @ManyToOne
    @NotNull
    private User user;

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(nullable = false)
    private String name;

    @Nullable
    private String description;

    @Nullable
    @Column(updatable = false)
    private Date dataBegin;

    @Nullable
    @Column(updatable = false)
    private Date dataEnd;

    public Task(
            @NotNull String name,
            @NotNull Project project,
            @NotNull User user
    ) {
        this.name = name;
        this.project = project;
        this.user = user;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}