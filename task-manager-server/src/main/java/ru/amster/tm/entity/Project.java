package ru.amster.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Project implements Serializable {

    @Nullable
    @OneToMany(
            mappedBy = "project",
            cascade = {CascadeType.ALL},
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

    @ManyToOne
    @NotNull
    private User user;

    @Id
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(nullable = false)
    private String name;

    @Nullable
    private String description;

    @Nullable
    @Column(updatable = false)
    private Date dataBegin;

    @Nullable
    @Column(updatable = false)
    private Date dataEnd;

    public Project(@NotNull String name, @NotNull User user) {
        this.user = user;
        this.name = name;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}