package ru.amster.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class Session implements Cloneable, Serializable {

    @ManyToOne
    private User user;

    @Id
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(nullable = false)
    private Long timestamp;

    @Nullable
    private String signature;

    public Session(User user) {
        this.user = user;
    }

    @Override
    @Nullable
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}