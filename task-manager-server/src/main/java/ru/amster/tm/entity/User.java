package ru.amster.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.enamuration.Role;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_user")
@Entity
public class User implements Serializable {

    @Nullable
    @OneToMany(
            mappedBy = "user",
            cascade = {CascadeType.ALL},
            orphanRemoval = true
    )
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Session> sessions = new ArrayList<>();

    @Id
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(nullable = false)
    private String login;

    @NotNull
    @Column(nullable = false)
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String fistName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private Role role = Role.USER;

    @Nullable
    @Column(nullable = false)
    private Boolean Locked = false;

    @Override
    public String toString() {
        return getId();
    }

}