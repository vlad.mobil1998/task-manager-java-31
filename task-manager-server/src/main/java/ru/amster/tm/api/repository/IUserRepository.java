package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    void removeById(@NotNull String id);

    @NotNull
    void removeByLogin(@NotNull String login);

    void merge(@NotNull User record);

    void removeAll();

    @NotNull
    List<User> getEntity();

    void remove(@NotNull User user);

}