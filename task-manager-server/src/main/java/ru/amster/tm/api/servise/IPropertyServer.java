package ru.amster.tm.api.servise;

public interface IPropertyServer {

    void init() throws Exception;

    String getServerHost();

    Integer getServerPort();

    String getSecretKey();

    String getJdbcDriver();

    String getJdbcUrl();

    String getUrlTest();

    String getJdbcUsername();

    String getJdbcPassword();

}