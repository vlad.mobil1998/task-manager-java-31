package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    @Nullable
    List<Project> findAll(@NotNull String userId);

    void removeAll(String userId);

    @Nullable
    Project findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    void removeOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    void removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Long numberOfAllProjects(@NotNull String userId);

    @NotNull List<Project> getEntity();

    void remove(@NotNull Project project);

    void merge(@NotNull Project record);

    void removeAll();

}