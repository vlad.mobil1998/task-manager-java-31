package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    void create(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String name
    );

    void create(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    Task findOneByName(
            @Nullable String userId,
            @Nullable String name
    );

    void removeOneByName(
            @Nullable String userId,
            @Nullable String name
    );

    void removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Long numberOfAllTasks(@Nullable String userId);

    void load(@NotNull List<Task> tasks);

    @Nullable
    List<Task> export();

    @Nullable
    List<Task> findAll();

    void add(@Nullable Task task);

    void clear();

}