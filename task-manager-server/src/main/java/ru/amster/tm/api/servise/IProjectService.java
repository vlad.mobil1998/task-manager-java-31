package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project findOneByName(@Nullable String userId, @Nullable String name);

    void removeOneByName(@Nullable String userId, @Nullable String name);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    Long numberOfAllProjects(@Nullable String userId);

    void load(@NotNull List<Project> projects);

    @Nullable
    List<Project> export();

    @Nullable
    List<Project> findAll();

    void add(@Nullable Project project);

    void clear();

}