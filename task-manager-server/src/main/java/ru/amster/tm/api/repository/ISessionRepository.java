package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {

    List<Session> findByUserId(@Nullable String userId);

    void removeByUserId(@NotNull String userId);

    Boolean contains(@NotNull String id);

    @NotNull List<Session> getEntity();

    void remove(@NotNull Session session);

    void merge(@NotNull Session record);

    void removeAll();

}