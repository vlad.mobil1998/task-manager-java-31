package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Session;

import java.util.List;

public interface ISessionService {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    String open(@NotNull String login, @NotNull String password);

    void close(@NotNull String sessionSecret);

    Session validate(@Nullable String session);

    Session sign(@NotNull Session session);

    Session validate(@NotNull String session, @Nullable Role role);

    void signOutByLogin(@Nullable String login);

    void signOutByUserId(@Nullable String userId);

    void load(@NotNull List<Session> sessions);

    @NotNull List<Session> export();

    @Nullable List<Session> findAll();

    void add(@Nullable Session session);

    void clear();

}