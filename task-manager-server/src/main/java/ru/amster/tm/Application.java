package ru.amster.tm;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.bootstrap.Bootstrap;

public final class Application {

    public static void main(@NotNull final String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}