package ru.amster.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.dto.UserDTO;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService
public final class UserEndpoint {

    IServiceLocator serviceLocator;

    public UserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public void createWithThreeParamUser(
            @WebParam(name = "log", partName = "log") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    ) {
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.create(login, password, role);
    }

    @WebMethod
    public void createWithFourParamUser(
            @WebParam(name = "log", partName = "log") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    ) {
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.create(login, password, email, role);
    }

    @WebMethod
    public void removeUser(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.removeById(session.getUser().getId());
        sessionService.close(sessionSecret);
    }

    @WebMethod
    @NotNull
    public UserDTO findUser(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        return new UserDTO(userService.findById(session.getUser().getId()));
    }

    @WebMethod
    public void updateUserEmail(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session =  sessionService.validate(sessionSecret);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.updateEmail(session.getUser().getId(), email);
    }

    @WebMethod
    public void updateUserFirstName(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.updateFirstName(session.getUser().getId(), firstName);
    }

    @WebMethod
    public void updateUserLastName(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.updateLastName(session.getUser().getId(), lastName);
    }

    @WebMethod
    public void updateUserMiddleName(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret,
            @WebParam(name = "middleName", partName = "middleName") @Nullable final String middleName
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.updateMiddleName(session.getUser().getId(), middleName);
    }

    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.updatePassword(session.getUser().getId(), password);
    }

}