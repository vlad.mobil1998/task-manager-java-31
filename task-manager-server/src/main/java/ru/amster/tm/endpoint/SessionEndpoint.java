package ru.amster.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class SessionEndpoint {

    private IServiceLocator serviceLocator;

    public SessionEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Nullable
    public String openSession(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        return sessionService.open(login, password);
    }

    @WebMethod
    public void closeSession(
            @WebParam(name = "session", partName = "session") @NotNull final String sessionSecret
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.close(sessionSecret);
    }

}