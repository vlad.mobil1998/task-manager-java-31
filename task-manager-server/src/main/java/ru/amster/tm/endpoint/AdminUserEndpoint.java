package ru.amster.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.*;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.dto.UserDTO;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@WebService
public final class AdminUserEndpoint {

    IServiceLocator serviceLocator;

    public AdminUserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @NotNull
    public void lockUserByLogin(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    )  {
        ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(sessionSecret, Role.ADMIN);
        IUserService userService = serviceLocator.getUserService();
        userService.lockUserByLogin(login);
    }

    @WebMethod
    @NotNull
    public void unlockUserByLogin(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    )  {
        ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(sessionSecret, Role.ADMIN);
        IUserService userService = serviceLocator.getUserService();
        userService.unlockUserByLogin(login);
    }

    @WebMethod
    public void load(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "domain", partName = "domain") @NotNull final Domain domain
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(sessionSecret, Role.ADMIN);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
    }

    @NotNull
    @WebMethod
    public Domain export(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(sessionSecret, Role.ADMIN);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        final Domain domain = new Domain();
        domainService.export(domain);
        return domain;
    }

    @WebMethod
    @Nullable
    public List<UserDTO> findAllUser(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(sessionSecret, Role.ADMIN);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        List<User> users = userService.findAll();
        List<UserDTO> userDTOList = new ArrayList<>();
        for (User user: users) {
            UserDTO userDTO = new UserDTO(user);
            userDTOList.add(userDTO);
        }
        return userDTOList;
    }

    @WebMethod
    @Nullable
    public UserDTO findUserById(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(sessionSecret, Role.ADMIN);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        return new UserDTO(userService.findById(id));
    }

    @WebMethod
    @Nullable
    public UserDTO findUserByLogin(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(sessionSecret, Role.ADMIN);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        return new UserDTO(userService.findByLogin(login));
    }

    @WebMethod
    public void removeUserById(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(sessionSecret, Role.ADMIN);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.removeById(id);
    }

    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(sessionSecret, Role.ADMIN);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.removeByLogin(login);
    }

}