package ru.amster.tm.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan("ru.amster.tm.listener")
@Configuration
public class CommandConfig {
}