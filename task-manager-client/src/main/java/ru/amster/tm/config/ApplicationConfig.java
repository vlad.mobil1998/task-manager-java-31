package ru.amster.tm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.amster.tm.dto.Session;
import ru.amster.tm.endpoint.*;
import ru.amster.tm.service.WebServiceLocator;

@Configuration
@ComponentScan({
        "ru.amster.tm.dto",
        "ru.amster.tm.service",
        "ru.amster.tm.bootstrap",
})
@Import(CommandConfig.class)
public class ApplicationConfig {

    @Autowired
    private WebServiceLocator webServiceLocator;

    @Autowired
    private Session session;

    @Bean
    public AdminUserEndpoint adminUserEndpoint() {
        return webServiceLocator.adminUserEndpoint();
    }

    @Bean
    public SessionEndpoint sessionEndpoint() {
        return webServiceLocator.sessionEndpoint();
    }

    @Bean
    public ProjectEndpoint projectEndpoint() {
        return webServiceLocator.projectEndpoint();
    }

    @Bean
    public TaskEndpoint taskEndpoint() {
        return webServiceLocator.taskEndpoint();
    }

    @Bean
    public UserEndpoint userEndpoint() {
        return webServiceLocator.userEndpoint();
    }

}