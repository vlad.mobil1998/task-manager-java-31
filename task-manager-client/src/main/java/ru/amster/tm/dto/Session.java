package ru.amster.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public class Session {

    @Getter
    @Setter
    private String sessionSecret;

}
