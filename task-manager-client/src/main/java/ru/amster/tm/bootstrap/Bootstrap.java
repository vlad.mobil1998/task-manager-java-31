package ru.amster.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.util.TerminalUtil;

@Component
public class Bootstrap {

    @Autowired
    private AbstractListener[] abstractListeners;

    @Autowired
    private ApplicationEventPublisher publisher;

    public final void run(@NotNull final String[] args) throws Exception {
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        String command = "";
        for (String arg : args) {
            publisher.publishEvent(new ConsoleEvent(arg));
        }
        while (true) {
            try {
                command = TerminalUtil.nextLine();
                ConsoleEvent event = new ConsoleEvent(command);
                publisher.publishEvent(event);
            } catch (Exception e) {
                System.err.println(e.toString());
                System.out.println("[FAIL]");
            }
        }
    }

}