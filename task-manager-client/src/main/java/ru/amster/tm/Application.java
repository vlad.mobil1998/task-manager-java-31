package ru.amster.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.amster.tm.bootstrap.Bootstrap;
import ru.amster.tm.config.ApplicationConfig;

public final class Application {

    public static void main(@NotNull final String[] args) throws Exception {
        final ApplicationContext context
                = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        context.getBean(Bootstrap.class).run(args);
    }

}