package ru.amster.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.endpoint.TaskDTO;
import ru.amster.tm.exception.empty.EmptyTaskException;

@UtilityClass
public class TaskUtil {

    @NotNull
    public void showTask(@Nullable final TaskDTO task) {
        if (task == null) throw new EmptyTaskException();
        System.out.println("ID: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

}