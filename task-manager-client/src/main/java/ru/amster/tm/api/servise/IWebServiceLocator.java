package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.endpoint.*;

public interface IWebServiceLocator {

    @NotNull
    AdminUserEndpoint adminUserEndpoint();

    @NotNull
    SessionEndpoint sessionEndpoint();

    @NotNull
    ProjectEndpoint projectEndpoint();

    @NotNull
    TaskEndpoint taskEndpoint();

    @NotNull
    UserEndpoint userEndpoint();

}