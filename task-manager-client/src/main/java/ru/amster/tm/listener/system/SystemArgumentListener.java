package ru.amster.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;

@Component
public final class SystemArgumentListener extends AbstractListener {

    @Autowired
    private AbstractListener[] abstractListeners;

    @Override
    @NotNull
    public String name() {
        return "arguments";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Display arguments program";
    }

    @Override
    @EventListener(condition = "@systemArgumentListener.name() == #event.getName()" +
            " || @systemArgumentListener.arg() == #event.getName()")
    public void handler(final ConsoleEvent event) {
        System.out.println("[ARGUMENTS]");
        for (AbstractListener argument : abstractListeners) {
            System.out.println(argument.arg());
        }
    }

}