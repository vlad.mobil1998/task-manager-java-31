package ru.amster.tm.listener.admin.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.admin.data.AbstractDataListener;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Component
public final class DataXmlClearListener extends AbstractDataListener {

    @Autowired
    private Session session;

    @Override
    @NotNull
    public String name() {
        return "data-xml-clear";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove xml data";
    }

    @Override
    @EventListener(condition = "@dataXmlClearListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) throws IOException, ClassNotFoundException {
        System.out.println("[DATA XML CLEAR]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        @NotNull final File file = new File(FILE_XML);
        Files.delete(file.toPath());
        System.out.println("[OK]");
    }

}