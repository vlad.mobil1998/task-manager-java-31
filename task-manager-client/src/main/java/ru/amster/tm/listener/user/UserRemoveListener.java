package ru.amster.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.user.AccessDeniedException;

@Component
public final class UserRemoveListener extends AbstractListener {

    @Autowired
    private Session session;

    @Autowired
    private UserEndpoint userEndpoint;

    @Override
    @NotNull
    public String name() {
        return "re-user";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Delete user by login";
    }

    @Override
    @EventListener(condition = "@userRemoveListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[DELETED USER]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();
        userEndpoint.removeUser(session.getSessionSecret());
        System.out.println("[OK]");
    }

}