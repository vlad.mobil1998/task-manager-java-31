package ru.amster.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;

@Component
public final class SystemVersionListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "version";
    }

    @Override
    @NotNull
    public String arg() {
        return "-v";
    }

    @Override
    @NotNull
    public String description() {
        return " - Show version info.";
    }

    @Override
    @EventListener(condition = "@systemVersionListener.name() == #event.getName()" +
            "|| @systemVersionListener.arg() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println("1.0.9");
    }

}