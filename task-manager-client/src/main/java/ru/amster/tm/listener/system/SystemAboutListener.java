package ru.amster.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;

@Component
public final class SystemAboutListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "about";
    }

    @Override
    @NotNull
    public String arg() {
        return "-a";
    }

    @Override
    @NotNull
    public String description() {
        return " - Show developer info.";
    }

    @Override
    @EventListener(condition = "@systemAboutListener.name() == #event.getName()" +
            " || @systemAboutListener.arg() == #event.getName()")
    public void handler(final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Amster Vladislav");
        System.out.println("E-MAIL: vlad@amster.ru");
    }

}