package ru.amster.tm.listener.user.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.util.TerminalUtil;

@Component
public final class LastNameUpdateListener extends AbstractListener {

    @Autowired
    private Session session;

    @Autowired
    private UserEndpoint userEndpoint;

    @Override
    @NotNull
    public String name() {
        return "upd-last-name";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - updating user last name";
    }

    @Override
    @EventListener(condition = "@lastNameUpdateListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[UPDATE LAST NAME]");

        System.out.println("ENTER LAST NAME");
        @Nullable final String lastName = TerminalUtil.nextLine();
        if (lastName == null || lastName.isEmpty()) throw new EmptyEmailException();

        userEndpoint.updateUserLastName(session.getSessionSecret(), lastName);
        System.out.println("[OK]");
    }

}