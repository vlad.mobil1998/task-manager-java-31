package ru.amster.tm.listener.user.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.util.TerminalUtil;

@Component
public final class MiddleNameUpdateListener extends AbstractListener {

    @Autowired
    private Session session;

    @Autowired
    private UserEndpoint userEndpoint;

    @Override
    @NotNull
    public String name() {
        return "upd-middle-name";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - updating user middle name";
    }

    @Override
    @EventListener(condition = "@middleNameUpdateListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[UPDATE MIDDLE NAME]");

        System.out.println("ENTER MIDDLE NAME");
        @Nullable final String middleName = TerminalUtil.nextLine();
        if (middleName == null || middleName.isEmpty()) throw new EmptyEmailException();

        userEndpoint.updateUserMiddleName(session.getSessionSecret(), middleName);
        System.out.println("[OK]");
    }

}