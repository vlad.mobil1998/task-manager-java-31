package ru.amster.tm.listener.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.SessionEndpoint;
import ru.amster.tm.exception.user.AccessDeniedException;

@Component
public final class LogoutListener extends AbstractListener {

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private Session session;

    @Override
    @NotNull
    public String name() {
        return "logout";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Log out system";
    }

    @Override
    @EventListener(condition = "@logoutListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[LOGOUT]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();
        sessionEndpoint.closeSession(session.getSessionSecret());
        session.setSessionSecret(null);
        System.out.println("[OK]");
    }

}