package ru.amster.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;

@Component
public final class SystemCommandListener extends AbstractListener {

    @Autowired
    private AbstractListener[] abstractListeners;

    @Override
    @NotNull
    public String name() {
        return "commands";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Display terminal command";
    }

    @Override
    @EventListener(condition = "@systemCommandListener.name() == #event.getName()" +
            "|| @systemCommandListener.arg() == #event.getName()")
    public void handler(final ConsoleEvent event) {
        System.out.println("[COMMANDS]");
        for (AbstractListener command : abstractListeners) {
            System.out.println(command.name());
        }
    }

}