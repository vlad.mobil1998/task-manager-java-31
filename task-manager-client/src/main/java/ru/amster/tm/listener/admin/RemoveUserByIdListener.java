package ru.amster.tm.listener.admin;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

@Component
public class RemoveUserByIdListener extends AbstractListener {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private Session session;

    @Override
    public String name() {
        return "re-user-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Remove user by id";
    }

    @Override
    @EventListener(condition = "@removeUserByIdListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[DELETED USER BY ID]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        System.out.println("ENTER ID");
        @Nullable final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyLoginException();

        adminUserEndpoint.removeUserById(session.getSessionSecret(), id);
        System.out.println("[OK]");
    }

}