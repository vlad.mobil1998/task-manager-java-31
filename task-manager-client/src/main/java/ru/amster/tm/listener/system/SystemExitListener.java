package ru.amster.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;

@Component
public final class SystemExitListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "exit";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Close application.";
    }

    @Override
    @EventListener(condition = "@systemExitListener.name() == #event.getName()" +
            "|| @systemExitListener.arg() == #event.getName()")
    public void handler(final ConsoleEvent event) {
        System.exit(0);
    }

}