package ru.amster.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.TaskDTO;
import ru.amster.tm.endpoint.TaskEndpoint;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TaskUtil;
import ru.amster.tm.util.TerminalUtil;

@Component
public final class TaskShowByNameListener extends AbstractListener {

    @Autowired
    private Session session;

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String name() {
        return "task-v-name";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show task by name";
    }

    @Override
    @EventListener(condition = "@taskShowByNameListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[SHOW TASK]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        System.out.println("ENTER NAME");
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final TaskDTO task = taskEndpoint.findTaskByName(session.getSessionSecret(), name);
        if (task == null) throw new EmptyTaskException();
        TaskUtil.showTask(task);
        System.out.println("[OK]");
    }

}