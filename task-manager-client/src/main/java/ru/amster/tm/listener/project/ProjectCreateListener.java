package ru.amster.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.ProjectEndpoint;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

@Component
public final class ProjectCreateListener extends AbstractListener {

    @Autowired
    private Session session;

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    @NotNull
    public String name() {
        return "project-create";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Create new project";
    }

    @Override
    @EventListener(condition = "@projectCreateListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[CREATE PROJECTS]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();

        projectEndpoint.createWithThreeParamProject(session.getSessionSecret(), name, description);
        System.out.println("[OK]");
    }

}