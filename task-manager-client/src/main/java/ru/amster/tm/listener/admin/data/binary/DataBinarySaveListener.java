package ru.amster.tm.listener.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.admin.data.AbstractDataListener;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.endpoint.Domain;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

@Component
public final class DataBinarySaveListener extends AbstractDataListener {

    @Autowired
    private Session session;

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Override
    @NotNull
    public String name() {
        return "data-bin-save";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Save data from binary file";
    }

    @Override
    @EventListener(condition = "@dataBinarySaveListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) throws IOException {
        System.out.println("[DATA BINARY SAVE]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        @NotNull final Domain domain = adminUserEndpoint.export(session.getSessionSecret());
        @NotNull final File file = new File(FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
        System.out.println("[OK]");
    }

}