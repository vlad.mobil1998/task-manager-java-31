package ru.amster.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.util.NumberUtil;

@Component
public final class SystemInfoListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "info";
    }

    @Override
    @NotNull
    public String arg() {
        return "-i";
    }

    @Override
    @NotNull
    public String description() {
        return " - Display information about system.";
    }

    @Override
    @EventListener(condition = "@systemInfoListener.name() == #event.getName()" +
            "|| @systemInfoListener.arg() == #event.getName()")
    public void handler(final ConsoleEvent event) {
        System.out.println("[INFO]");

        @NotNull final int availableProcessor = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessor);

        @NotNull final long freeMemory = Runtime.getRuntime().freeMemory();
        @NotNull final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        @NotNull final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);

        @NotNull final long totalMemory = Runtime.getRuntime().totalMemory();
        @NotNull final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        @NotNull final long usedMemory = totalMemory - freeMemory;
        @NotNull final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }

}