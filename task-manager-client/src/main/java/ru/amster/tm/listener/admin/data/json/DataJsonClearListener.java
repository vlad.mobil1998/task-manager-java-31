package ru.amster.tm.listener.admin.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.admin.data.AbstractDataListener;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Component
public final class DataJsonClearListener extends AbstractDataListener {

    @Autowired
    private Session session;

    @Override
    @NotNull
    public String name() {
        return "data-json-clear";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove json data";
    }

    @Override
    @EventListener(condition = "@dataJsonClearListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) throws IOException, ClassNotFoundException {
        System.out.println("[DATA JSON CLEAR]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        @NotNull final File file = new File(FILE_JSON);
        Files.delete(file.toPath());
        System.out.println("[OK]");
    }

}