package ru.amster.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.ProjectDTO;
import ru.amster.tm.endpoint.ProjectEndpoint;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.ProjectUtil;

import java.util.List;

@Component
public final class ProjectShowListener extends AbstractListener {

    @Autowired
    private Session session;

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    @NotNull
    public String name() {
        return "project-list";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show project list";
    }

    @Override
    @EventListener(condition = "@projectShowListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[LIST PROJECT]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        @Nullable final List<ProjectDTO> projects = projectEndpoint.findAllProject(
                session.getSessionSecret()
        );
        for (@Nullable final ProjectDTO project : projects)
            ProjectUtil.showProject(project);
        System.out.println("[OK]");
    }

}