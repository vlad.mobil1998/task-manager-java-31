package ru.amster.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.TaskDTO;
import ru.amster.tm.endpoint.TaskEndpoint;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TaskUtil;

import java.util.List;

@Component
public final class TaskShowListener extends AbstractListener {

    @Autowired
    private Session session;

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String name() {
        return "task-list";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show task list";
    }

    @Override
    @EventListener(condition = "@taskShowListener == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[LIST TASK]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        @Nullable final List<TaskDTO> tasks = taskEndpoint.findAllTask(session.getSessionSecret());
        for (@Nullable final TaskDTO task : tasks)
            TaskUtil.showTask(task);
        System.out.println("[OK]");
    }

}