package ru.amster.tm.listener.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.endpoint.UserDTO;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.List;

@Component
public final class AllUserShowListener extends AbstractListener {

    @Autowired
    private Session session;

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Override
    @NotNull
    public String name() {
        return "view-all-users";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show All Users";
    }

    @Override
    @EventListener(condition = "@allUserShowListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[SHOW ALL USER]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        @NotNull final List<UserDTO> users = adminUserEndpoint.findAllUser(session.getSessionSecret());
        for (@NotNull final UserDTO item : users) {
            System.out.println("[LOGIN]");
            System.out.println(item.getLogin());
            if (item.getEmail() != null && !item.getEmail().isEmpty()) {
                System.out.println("[EMAIL]");
                System.out.println(item.getEmail());
            }
            if (item.getFistName() != null && !item.getFistName().isEmpty()) {
                System.out.println("[FIST NAME]");
                System.out.println(item.getFistName());
            }
            if (item.getMiddleName() != null && !item.getMiddleName().isEmpty()) {
                System.out.println("[MIDDLE NAME]");
                System.out.println(item.getMiddleName());
            }
            if (item.getLastName() != null && !item.getLastName().isEmpty()) {
                System.out.println("[LAST NAME]");
                System.out.println(item.getLastName());
            }
            System.out.println("[OK]");
        }
    }

}