package ru.amster.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.UserDTO;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.empty.EmptyUserException;

@Component
public final class UserProfileShowListener extends AbstractListener {

    @Autowired
    private UserEndpoint userEndpoint;

    @Autowired
    private Session session;

    @Override
    @NotNull
    public String name() {
        return "view-profile";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show you profile";
    }

    @Override
    @EventListener(condition = "@userProfileShowListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[SHOW PROFILE]");
        @Nullable final UserDTO user = userEndpoint.findUser(session.getSessionSecret());
        if (user == null) throw new EmptyUserException();

        System.out.println("[LOGIN]");
        System.out.println(user.getLogin());
        if (user.getEmail() != null && !user.getEmail().isEmpty()) {
            System.out.println("[EMAIL]");
            System.out.println(user.getEmail());
        }
        if (user.getFistName() != null && !user.getFistName().isEmpty()) {
            System.out.println("[FIST NAME]");
            System.out.println(user.getFistName());
        }
        if (user.getMiddleName() != null && !user.getMiddleName().isEmpty()) {
            System.out.println("[MIDDLE NAME]");
            System.out.println(user.getMiddleName());
        }
        if (user.getLastName() != null && !user.getLastName().isEmpty()) {
            System.out.println("[LAST NAME]");
            System.out.println(user.getLastName());
        }
        System.out.println("OK");
    }

}