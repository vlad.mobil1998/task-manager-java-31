package ru.amster.tm.listener.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

@Component
public final class UserUnlockListener extends AbstractListener {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private Session session;

    @Override
    @NotNull
    public String name() {
        return "unlock-user";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Unlock user by login";
    }

    @Override
    @EventListener(condition = "@userUnlockListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[LOCK USER]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        System.out.println("ENTER LOGIN");
        @Nullable final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        adminUserEndpoint.unlockUserByLogin(session.getSessionSecret(), login);
        System.out.println("[OK]");
    }

}