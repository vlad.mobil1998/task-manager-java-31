package ru.amster.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.ProjectDTO;
import ru.amster.tm.endpoint.ProjectEndpoint;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.ProjectUtil;
import ru.amster.tm.util.TerminalUtil;

@Component
public final class ProjectShowByNameListener extends AbstractListener {

    @Autowired
    private Session session;

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    @NotNull
    public String name() {
        return "project-v-name";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show project by name";
    }

    @Override
    @EventListener(condition = "@projectShowByNameListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[SHOW PROJECT]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        System.out.println("ENTER NAME");
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final ProjectDTO project = projectEndpoint.findProjectByName(
                session.getSessionSecret(),
                name
        );
        if (project == null) throw new EmptyProjectException();
        ProjectUtil.showProject(project);
        System.out.println("[OK]");
    }

}