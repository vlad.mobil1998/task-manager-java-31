package ru.amster.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.TaskEndpoint;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractListener {

    @Autowired
    private Session session;

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String name() {
        return "task-create";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Create new task";
    }

    @Override
    @EventListener(condition = "@taskCreateListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[CREATE TASKS]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();

        System.out.println("ENTER NAME PROJECT:");
        @Nullable final String nameProject = TerminalUtil.nextLine();
        if (nameProject == null || nameProject.isEmpty()) throw new EmptyNameException();

        taskEndpoint.createWithThreeParamTask(
                session.getSessionSecret(),
                name,
                description,
                nameProject
        );
        System.out.println("[OK]");
    }

}