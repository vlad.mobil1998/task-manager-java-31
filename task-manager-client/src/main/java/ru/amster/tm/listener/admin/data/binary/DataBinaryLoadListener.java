package ru.amster.tm.listener.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.admin.data.AbstractDataListener;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.endpoint.Domain;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

@Component
public final class DataBinaryLoadListener extends AbstractDataListener {

    @Autowired
    private Session session;

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Override
    @NotNull
    public String name() {
        return "data-bin-load";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Load data to binary file";
    }

    @Override
    @EventListener(condition = "@dataBinaryLoadListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) throws IOException, ClassNotFoundException {
        System.out.println("[DATA BINARY LOAD]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();

        adminUserEndpoint.load(session.getSessionSecret(), domain);
        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");
    }

}