package ru.amster.tm.listener.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.Role;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptyPasswordException;
import ru.amster.tm.util.TerminalUtil;

@Component
public final class RegistrationListener extends AbstractListener {

    @Autowired
    private UserEndpoint userEndpoint;

    @Override
    @NotNull
    public String name() {
        return "reg";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Register now";
    }

    @Override
    @EventListener(condition = "@registrationListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[REGISTRATION]");

        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        System.out.println("[ENTER PASSWORD]");
        @Nullable final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        System.out.println("[ENTER EMAIL]");
        @Nullable final String email = TerminalUtil.nextLine();
        userEndpoint.createWithFourParamUser(login, password, email, Role.USER);

        System.out.println("[OK]");
    }

}