package ru.amster.tm.listener.user.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.util.TerminalUtil;

@Component
public final class EmailUpdateListener extends AbstractListener {

    @Autowired
    private Session session;

    @Autowired
    private UserEndpoint userEndpoint;

    @Override
    @NotNull
    public String name() {
        return "upd-email";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - updating user email";
    }

    @Override
    @EventListener(condition = "@emailUpdateListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[UPDATE EMAIL]");

        System.out.println("ENTER EMAIL");
        @Nullable final String email = TerminalUtil.nextLine();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();

        userEndpoint.updateUserEmail(session.getSessionSecret(), email);
        System.out.println("[OK]");
    }

}