package ru.amster.tm.listener;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.amster.tm.api.servise.IWebServiceLocator;
import ru.amster.tm.event.ConsoleEvent;

import java.io.IOException;

@NoArgsConstructor
public abstract class AbstractListener {

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void handler(ConsoleEvent event) throws IOException, ClassNotFoundException;

}