package ru.amster.tm.listener.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;
import ru.amster.tm.endpoint.SessionEndpoint;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptyPasswordException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

@Component
public final class LoginListener extends AbstractListener {

    @Autowired
    private Session session;

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    @NotNull
    public String name() {
        return "login";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Sign in system";
    }

    @Override
    @EventListener(condition = "@loginListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        System.out.println("[ENTER PASSWORD]");
        @Nullable final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        String sessionReceived = sessionEndpoint.openSession(login, password);
        if (sessionReceived == null) throw new AccessDeniedException();
        session.setSessionSecret(sessionReceived);
        System.out.println("[OK]");
    }

}