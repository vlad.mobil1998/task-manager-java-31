package ru.amster.tm.listener.admin.data;

import lombok.NoArgsConstructor;
import ru.amster.tm.listener.AbstractListener;

@NoArgsConstructor
public abstract class AbstractDataListener extends AbstractListener {

    protected static final String FILE_BINARY = "./data.bin";

    protected static final String FILE_XML = "./data.xml";

    protected static final String FILE_JSON = "./data.json";

    protected static final String FILE_BASE64 = "./data.base64";

}