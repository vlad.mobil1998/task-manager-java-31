package ru.amster.tm.listener.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.AbstractListener;

@Component
@NoArgsConstructor
public final class SystemHelpListener extends AbstractListener {

    @Autowired
    private AbstractListener[] abstractListeners;

    @Override
    @NotNull
    public String name() {
        return "help";
    }

    @Override
    @NotNull
    public String arg() {
        return "-h";
    }

    @Override
    @NotNull
    public String description() {
        return " - Display terminal commands.";
    }

    @Override
    @EventListener(condition = "@systemHelpListener.name() == #event.getName()" +
            "|| @systemHelpListener.arg() == #event.getName()")
    public void handler(final ConsoleEvent event) {
        System.out.println("[HELP]");
        for (AbstractListener command : abstractListeners) {
            if (command.arg() == null) System.out.println(
                    command.name()
                            + " " + command.description()
            );
            else System.out.println(
                    command.name() + " "
                            + command.arg()
                            + command.description()
            );
        }
    }

}