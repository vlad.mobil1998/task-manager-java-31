package ru.amster.tm.listener.admin.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.amster.tm.dto.Session;
import ru.amster.tm.event.ConsoleEvent;
import ru.amster.tm.listener.admin.data.AbstractDataListener;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.endpoint.Domain;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

@Component
public final class DataJsonSaveListener extends AbstractDataListener {

    @Autowired
    private Session session;

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Override
    @NotNull
    public String name() {
        return "data-json-save";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Save data from json file";
    }

    @Override
    @EventListener(condition = "@dataJsonSaveListener.name() == #event.getName()")
    public void handler(ConsoleEvent event) throws IOException, ClassNotFoundException {
        System.out.println("[DATA JSON SAVE]");
        if (session.getSessionSecret() == null) throw new AccessDeniedException();

        @NotNull final File file = new File(FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final Domain domain = adminUserEndpoint.export(session.getSessionSecret());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[OK]");
    }

}