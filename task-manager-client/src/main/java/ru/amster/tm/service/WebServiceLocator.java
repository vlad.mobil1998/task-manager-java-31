package ru.amster.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.amster.tm.api.servise.IWebServiceLocator;
import ru.amster.tm.endpoint.*;

@NoArgsConstructor
@Component
public final class WebServiceLocator implements IWebServiceLocator {

    public AdminUserEndpointService adminUserEndpointService() {
        return new AdminUserEndpointService();
    }

    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Override
    @NotNull
    public final AdminUserEndpoint adminUserEndpoint() {
        return adminUserEndpointService().getAdminUserEndpointPort();
    }

    @Override
    @NotNull
    public final SessionEndpoint sessionEndpoint() {
        return sessionEndpointService().getSessionEndpointPort();
    }

    @Override
    @NotNull
    public final ProjectEndpoint projectEndpoint() {
        return projectEndpointService().getProjectEndpointPort();
    }

    @Override
    @NotNull
    public final TaskEndpoint taskEndpoint() {
        return taskEndpointService().getTaskEndpointPort();
    }

    @Override
    @NotNull
    public final UserEndpoint userEndpoint() {
        return userEndpointService().getUserEndpointPort();
    }

}