
package ru.amster.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for export complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="export"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sessionSecret" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "export", propOrder = {
        "sessionSecret"
})
public class Export {

    protected String sessionSecret;

    /**
     * Gets the value of the sessionSecret property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSessionSecret() {
        return sessionSecret;
    }

    /**
     * Sets the value of the sessionSecret property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSessionSecret(String value) {
        this.sessionSecret = value;
    }

}
