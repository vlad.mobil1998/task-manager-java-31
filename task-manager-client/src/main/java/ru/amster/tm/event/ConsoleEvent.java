package ru.amster.tm.event;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public final class ConsoleEvent {

    private String name;

    public ConsoleEvent(String name) {
        this.name = name;
    }

}
