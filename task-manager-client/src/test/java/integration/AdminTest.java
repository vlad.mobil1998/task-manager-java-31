package integration;

import marker.IntegrationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.amster.tm.endpoint.*;
import ru.amster.tm.service.WebServiceLocator;

import java.util.List;

@Category(IntegrationTest.class)
@Ignore
public class AdminTest {

    private static WebServiceLocator webServiceLocator;

    private static AdminUserEndpoint adminUserEndpoint;

    private static SessionEndpoint sessionEndpoint;

    private static UserEndpoint userEndpoint;

    private static String session;

    @BeforeClass
    public static void init() {
        webServiceLocator = new WebServiceLocator();
        adminUserEndpoint = webServiceLocator.adminUserEndpoint();
        sessionEndpoint = webServiceLocator.sessionEndpoint();
        userEndpoint = webServiceLocator.userEndpoint();

        session = sessionEndpoint.openSession("admin", "admin");
    }

    @AfterClass
    public static void exit() {

        sessionEndpoint.closeSession(session);
    }

    @Test
    public void allUSerShowTest() {
        List<UserDTO> users = adminUserEndpoint.findAllUser(session);
        Assert.assertNotNull(users);
        Assert.assertEquals(users.get(0).getLogin(), "test");
        Assert.assertEquals(users.get(1).getLogin(), "test1");
        Assert.assertEquals(users.get(2).getLogin(), "admin");
    }

    @Test
    public void removeUserTest() {
        userEndpoint.createWithThreeParamUser("temp", "temp", Role.USER);
        userEndpoint.createWithThreeParamUser("temp1", "temp1", Role.USER);

        String sessionTemp = sessionEndpoint.openSession("temp1", "temp1");
        UserDTO user = userEndpoint.findUser(sessionTemp);

        adminUserEndpoint.removeUserByLogin(session, "temp");
        adminUserEndpoint.removeUserById(session, user.getId());

        List<UserDTO> users = adminUserEndpoint.findAllUser(session);
        Assert.assertEquals(3, users.size());
    }

    @Test
    public void lockAndUnlockTest() {
        adminUserEndpoint.lockUserByLogin(session, "test");
        UserDTO user = adminUserEndpoint.findUserByLogin(session, "test");
        Assert.assertTrue(user.isLocked());
        adminUserEndpoint.unlockUserByLogin(session, "test");
        user = adminUserEndpoint.findUserByLogin(session, "test");
        Assert.assertFalse(user.isLocked());
    }

}